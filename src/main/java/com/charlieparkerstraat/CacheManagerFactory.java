/*
 * Copyright (C) 2017 angelo.ndira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.charlieparkerstraat;

import static com.charlieparkerstraat.ApplicationConstants.KEY_EHCACHE_CONFIG_FILE;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServer;
import javax.servlet.ServletContext;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.management.ManagementService;

public class CacheManagerFactory {

    private static final Object CACHE_CREATE_MONITOR = new Object();
    private static final Logger LOG = Logger.getLogger(CacheManagerFactory.class.getName());
    private static CacheManager cacheManager;

    static void createCacheManager(final ServletContext servletContext) {
        final String ehcacheConfigFilename = servletContext.getInitParameter(KEY_EHCACHE_CONFIG_FILE);
        final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(ehcacheConfigFilename);

        synchronized (CacheManagerFactory.class) {
            if (cacheManager == null) {
                cacheManager = CacheManager.newInstance(inputStream);
                MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
                ManagementService.registerMBeans(cacheManager, mBeanServer, false, false, false, true);
            }
        }
    }

    public static Cache getCache(final String name) {
        Cache cache;
        final CacheManager cacheManagerRef = CacheManagerFactory.getCacheManager();
        if (cacheManagerRef == null) {
            LOG.log(Level.INFO, "Cache manager is null (not set)");
            return null;
        }
        synchronized (CACHE_CREATE_MONITOR) {
            cache = cacheManagerRef.getCache(name);
            if (cache == null) {
                cacheManagerRef.addCache(name);
                cache = cacheManagerRef.getCache(name);
            }
        }
        return cache;
    }

    static CacheManager getCacheManager() {
        synchronized (CacheManagerFactory.class) {
            return cacheManager;
        }
    }

    static void logCacheNames() {
        final CacheManager cacheManagerCopy;
        synchronized (CacheManagerFactory.class) {
            cacheManagerCopy = cacheManager;
        }
        final String[] cacheNames = cacheManagerCopy.getCacheNames();
        LOG.log(Level.INFO, "Cache Names: {0}", Arrays.asList(cacheNames));
    }

    static void shutdownCacheManager() {
        synchronized (CacheManagerFactory.class) {
            if (cacheManager != null) {
                cacheManager.shutdown();
            }
        }
    }

    private CacheManagerFactory() {
    }

}
