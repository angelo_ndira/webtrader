/*
 * Copyright (C) 2017
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.charlieparkerstraat.betfair;

import com.betfair.sports.api.MarketBettingType;
import com.betfair.sports.api.MarketFilter;
import com.betfair.sports.api.TimeGranularity;
import com.betfair.sports.api.VenueResult;
import com.charlieparkerstraat.Action;
import com.charlieparkerstraat.ActionContext;
import static com.charlieparkerstraat.ApplicationConstants.KEY_BETFAIR_EVENT_TYPE_IDS;
import static com.charlieparkerstraat.ApplicationConstants.KEY_BETFAIR_EXCHANGE_IDS;
import static com.charlieparkerstraat.ApplicationConstants.KEY_BETFAIR_LOCALE;
import static com.charlieparkerstraat.ApplicationConstants.KEY_BETFAIR_MARKET_BETTING_TYPES;
import static com.charlieparkerstraat.ApplicationConstants.KEY_BETFAIR_TURNING_IN_PLAY;
import com.charlieparkerstraat.BaseJob;
import com.charlieparkerstraat.Dispatcher;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.charlieparkerstraat.betfair.client.BetfairClient;
import org.charlieparkerstraat.betfair.client.BetfairClientParameters;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;

public class ListVenuesJob extends BaseJob {

    private static final Logger LOG = Logger.getLogger(ListVenuesJob.class.getName());
    private final Function<String, MarketBettingType> STRING_TO_MARKET_TYPE_CONVERTER = (String string) -> MarketBettingType.valueOf(string);
    private final Function<String, String> STRING_TO_STRING_CONVERTER = (String input) -> input;

    @Override
    public Action getAction() {
        return (final ActionContext actionContext) -> {
            final BetfairClient<BetfairClientParameters> client = actionContext.getNonNullClient();
            final JobDataMap jobDataMap = actionContext.getContext().getJobDetail().getJobDataMap();
            final Set<String> exchangeIds = stringToSet(jobDataMap.getString(KEY_BETFAIR_EXCHANGE_IDS));
            final Set<String> eventTypeIds = stringToSet(jobDataMap.getString(KEY_BETFAIR_EVENT_TYPE_IDS));
            final boolean turnInPlayEnabled = jobDataMap.getBoolean(KEY_BETFAIR_TURNING_IN_PLAY);
            final Set<MarketBettingType> marketBettingTypes = this.<MarketBettingType>stringToSet(jobDataMap.getString(KEY_BETFAIR_MARKET_BETTING_TYPES), STRING_TO_MARKET_TYPE_CONVERTER);
            final String locale = jobDataMap.getString(KEY_BETFAIR_LOCALE);
            final MarketFilter filter = new MarketFilter();
            filter.setExchangeIds(exchangeIds);
            filter.setEventTypeIds(eventTypeIds);
            filter.setTurnInPlayEnabled(turnInPlayEnabled);
            filter.setMarketBettingTypes(marketBettingTypes);
            TimeGranularity granularity = TimeGranularity.MINUTES;
            final List<VenueResult> venueResultList;
            try {
                venueResultList = client.listVenues(filter);
                final Dispatcher dispatcher = Dispatcher.getInstance(actionContext.getServletContext(), actionContext.getScheduler());
                for (VenueResult venueResult : venueResultList) {
                    dispatcher.dispatch(venueResult.getVenue(), venueResult, VenueResult.class);
                }
            } catch (Throwable ex) {
                LOG.log(Level.SEVERE, null, ex);
                throw new JobExecutionException(ex.getMessage());
            }
        };
    }

    private Set<String> stringToSet(String string) {
        return this.<String>stringToSet(string, STRING_TO_STRING_CONVERTER);
    }

    private <R> Set<R> stringToSet(String string, Function<String, R> function) {
        final Set<R> set = new HashSet<>(0);
        final StringTokenizer tokenizer = new StringTokenizer(string, ",;");
        while (tokenizer.hasMoreTokens()) {
            set.add(function.apply(tokenizer.nextToken()));
        }
        return set;
    }
}
