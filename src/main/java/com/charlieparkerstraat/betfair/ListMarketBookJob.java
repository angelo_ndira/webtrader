/*
 * Copyright (C) 2017
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.charlieparkerstraat.betfair;

import com.betfair.sports.api.ExBestOffersOverrides;
import com.betfair.sports.api.MarketBook;
import com.betfair.sports.api.MarketCatalogue;
import com.betfair.sports.api.MatchProjection;
import com.betfair.sports.api.OrderProjection;
import com.betfair.sports.api.PriceData;
import com.betfair.sports.api.PriceProjection;
import com.betfair.sports.api.RollupModel;
import com.charlieparkerstraat.Action;
import com.charlieparkerstraat.ActionContext;
import static com.charlieparkerstraat.ApplicationConstants.KEY_BEST_PRICE_DEPTH;
import static com.charlieparkerstraat.ApplicationConstants.KEY_BETFAIR_MAXIMUM_RESULTS;
import static com.charlieparkerstraat.ApplicationConstants.KEY_BETFAIR_PRICE_DATA;
import static com.charlieparkerstraat.ApplicationConstants.KEY_MATCH_PROJECTION;
import static com.charlieparkerstraat.ApplicationConstants.KEY_ORDER_PROJECTION;
import static com.charlieparkerstraat.ApplicationConstants.KEY_ROLL_OVER_STAKES;
import static com.charlieparkerstraat.ApplicationConstants.KEY_ROLL_UP_LIABILITY_FACTOR;
import static com.charlieparkerstraat.ApplicationConstants.KEY_ROLL_UP_LIABILITY_THRESHOLD;
import static com.charlieparkerstraat.ApplicationConstants.KEY_ROLL_UP_LIMIT;
import static com.charlieparkerstraat.ApplicationConstants.KEY_ROLL_UP_MODEL;
import static com.charlieparkerstraat.ApplicationConstants.KEY_VIRTUALISE;
import com.charlieparkerstraat.BaseJob;
import com.charlieparkerstraat.CacheManagerFactory;
import com.charlieparkerstraat.Dispatcher;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Element;
import org.charlieparkerstraat.betfair.client.BetfairClient;
import org.charlieparkerstraat.betfair.client.BetfairClientParameters;
import org.quartz.JobDataMap;

public class ListMarketBookJob extends BaseJob {

    private static final Logger LOG = Logger.getLogger(ListMarketBookJob.class.getName());
    private final Function<String, PriceData> STRING_TO_PRICE_DATA_CONVERTER = (String string) -> PriceData.valueOf(string);
    private final long TEN_SECONDS = 10 * 1000L;

    private List<List<String>> createMarketIdBatches(final int maxRequestMarketIds) throws CacheException, IllegalStateException {
        final List<List<String>> batches = new ArrayList<>(0);
        final Cache catalogueCache = CacheManagerFactory.getCache(MarketCatalogue.class.getName());
        if (catalogueCache == null) {
            LOG.log(Level.WARNING, "market catalogue cache is null (not set)");
            return batches;
        }
        final List<?> keys = catalogueCache.getKeys();
        final Map<Object, Element> catalogueElements = catalogueCache.getAll(keys);
        if (catalogueElements == null || catalogueElements.isEmpty()) {
            LOG.log(Level.WARNING, "market catalogue cache is empty (no markets found)");
            return batches;
        }
        List<String> marketIds = new ArrayList<>(0);
        for (Object object : catalogueElements.values()) {
            final MarketCatalogue catalogue = (MarketCatalogue) ((Element) object).getObjectValue();
            marketIds.add(catalogue.getMarketId());
            if (marketIds.size() >= maxRequestMarketIds) {
                batches.add(marketIds);
                marketIds = new ArrayList<>(0);
            }
        }
        if (!marketIds.isEmpty()) {
            batches.add(marketIds);
        }
        return batches;
    }

    @Override
    public Action getAction() {
        LOG.log(Level.WARNING, "Starting job");
        return (final ActionContext actionContext) -> {
            final BetfairClient<BetfairClientParameters> client = actionContext.getNonNullClient();
            final JobDataMap jobDataMap = actionContext.getContext().getJobDetail().getJobDataMap();
            final Set<PriceData> priceData = this.<PriceData>stringToSet(jobDataMap.getString(KEY_BETFAIR_PRICE_DATA), STRING_TO_PRICE_DATA_CONVERTER);
            Integer bestPricesDepth = jobDataMap.getInt(KEY_BEST_PRICE_DEPTH);
            RollupModel rollupModel = RollupModel.valueOf(jobDataMap.getString(KEY_ROLL_UP_MODEL));
            Integer rollupLimit = jobDataMap.getInt(KEY_ROLL_UP_LIMIT);
            Double rollupLiabilityThreshold = jobDataMap.getDouble(KEY_ROLL_UP_LIABILITY_THRESHOLD);
            Integer rollupLiabilityFactor = jobDataMap.getInt(KEY_ROLL_UP_LIABILITY_FACTOR);
            ExBestOffersOverrides exBestOffersOverrides = new ExBestOffersOverrides(bestPricesDepth, rollupModel, rollupLimit, rollupLiabilityThreshold, rollupLiabilityFactor);
            Boolean virtualise = jobDataMap.getBoolean(KEY_VIRTUALISE);
            Boolean rolloverStakes = jobDataMap.getBoolean(KEY_ROLL_OVER_STAKES);
            final PriceProjection priceProjection = new PriceProjection(priceData, exBestOffersOverrides, virtualise, rolloverStakes);
            final String currencyCode = null;
            final int maximumResults = jobDataMap.getInt(KEY_BETFAIR_MAXIMUM_RESULTS);
            final OrderProjection orderProjection = OrderProjection.valueOf(jobDataMap.getString(KEY_ORDER_PROJECTION));
            final MatchProjection matchProjection = MatchProjection.valueOf(jobDataMap.getString(KEY_MATCH_PROJECTION));
            List<List<String>> batches = createMarketIdBatches(maximumResults);
            try {
                for (List<String> batch : batches) {
                    final List<MarketBook> marketBookList = client.listMarketBook(batch, priceProjection, orderProjection, matchProjection, currencyCode);
                    final Dispatcher dispatcher = Dispatcher.getInstance(actionContext.getServletContext(), actionContext.getScheduler());
                    for (MarketBook marketBook : marketBookList) {
                        dispatcher.dispatch(marketBook.getMarketId(), marketBook, MarketBook.class);
                    }
                }
            } catch (Throwable ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        };
    }

    private <R> Set<R> stringToSet(String string, Function<String, R> function) {
        final Set<R> set = new HashSet<>(0);
        if (string == null || string.trim().isEmpty()) {
            return set;
        }
        final StringTokenizer tokenizer = new StringTokenizer(string, ",;");
        while (tokenizer.hasMoreTokens()) {
            set.add(function.apply(tokenizer.nextToken()));
        }
        return set;
    }
}
