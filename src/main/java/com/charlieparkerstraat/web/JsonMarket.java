/*
 * Copyright (C) 2017 angelo.ndira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.charlieparkerstraat.web;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Logger;

public class JsonMarket implements Serializable {

    private static final Logger LOG = Logger.getLogger(JsonMarket.class.getName());
    private static final long serialVersionUID = 1L;
    private String competitionId;
    private String competitionName;
    private String countryCode;
    private String eventId;
    private String eventName;
    private Date eventOpenDate;
    private String eventTimeZone;
    private String eventTypeId;
    private String eventTypeName;
    private Integer marketBookBetDelay;
    private Boolean marketBookComplete;
    private Boolean marketBookCrossMatching;
    private Boolean marketBookDelayedMarketData;
    private Boolean marketBookInPlay;
    private String marketBookStatus;
    private Double marketBookTotalAvailable;
    private Double marketBookTotalMatched;
    private String marketId;
    private String marketName;
    private Date marketStartTime;
    private Double team1BackPrice;
    private Double team1BackVol;
    private Double team1LastPriceTraded;
    private Double team1LayPrice;
    private Double team1LayVol;
    private String team1Name;
    private Long team1SelectionId;
    private String team1Status;
    private Double team1TotalMatched;
    private Double team1TradedPrice;
    private Double team1TradedVol;
    private Double team2BackPrice;
    private Double team2BackVol;
    private Double team2LastPriceTraded;
    private Double team2LayPrice;
    private Double team2LayVol;
    private String team2Name;
    private Long team2SelectionId;
    private String team2Status;
    private Double team2TotalMatched;
    private Double team2TradedPrice;
    private Double team2TradedVol;
    private Double teamXBackPrice;
    private Double teamXBackVol;
    private Double teamXLastPriceTraded;
    private Double teamXLayPrice;
    private Double teamXLayVol;
    private String teamXName;
    private Long teamXSelectionId;
    private String teamXStatus;
    private Double teamXTotalMatched;
    private Double teamXTradedPrice;
    private Double teamXTradedVol;
    private Double totalMatched;
    private String venue;

    @Override
    public boolean equals(Object object) {
        if (object != null && object instanceof JsonMarket) {
            final JsonMarket other = (JsonMarket) object;
            if (marketId != null) {
                return marketId.equals(other.getMarketId());
            } else if (other.getMarketId() == null) {
                return true;
            }
        }
        return false;
    }

    public String getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(String competitionId) {
        this.competitionId = competitionId;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getEventOpenDate() {
        return eventOpenDate;
    }

    public void setEventOpenDate(Date eventOpenDate) {
        this.eventOpenDate = eventOpenDate;
    }

    public String getEventTimeZone() {
        return eventTimeZone;
    }

    public void setEventTimeZone(String eventTimeZone) {
        this.eventTimeZone = eventTimeZone;
    }

    public String getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(String eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public String getEventTypeName() {
        return eventTypeName;
    }

    public void setEventTypeName(String eventTypeName) {
        this.eventTypeName = eventTypeName;
    }

    public Integer getMarketBookBetDelay() {
        return marketBookBetDelay;
    }

    public void setMarketBookBetDelay(Integer marketBookBetDelay) {
        this.marketBookBetDelay = marketBookBetDelay;
    }

    public Boolean getMarketBookComplete() {
        return marketBookComplete;
    }

    public void setMarketBookComplete(Boolean marketBookComplete) {
        this.marketBookComplete = marketBookComplete;
    }

    public Boolean getMarketBookCrossMatching() {
        return marketBookCrossMatching;
    }

    public void setMarketBookCrossMatching(Boolean marketBookCrossMatching) {
        this.marketBookCrossMatching = marketBookCrossMatching;
    }

    public Boolean getMarketBookDelayedMarketData() {
        return marketBookDelayedMarketData;
    }

    public void setMarketBookDelayedMarketData(Boolean marketBookDelayedMarketData) {
        this.marketBookDelayedMarketData = marketBookDelayedMarketData;
    }

    public Boolean getMarketBookInPlay() {
        return marketBookInPlay;
    }

    public void setMarketBookInPlay(Boolean marketBookInPlay) {
        this.marketBookInPlay = marketBookInPlay;
    }

    public String getMarketBookStatus() {
        return marketBookStatus;
    }

    public void setMarketBookStatus(String marketBookStatus) {
        this.marketBookStatus = marketBookStatus;
    }

    public Double getMarketBookTotalAvailable() {
        return marketBookTotalAvailable;
    }

    public void setMarketBookTotalAvailable(Double marketBookTotalAvailable) {
        this.marketBookTotalAvailable = marketBookTotalAvailable;
    }

    public Double getMarketBookTotalMatched() {
        return marketBookTotalMatched;
    }

    public void setMarketBookTotalMatched(Double marketBookTotalMatched) {
        this.marketBookTotalMatched = marketBookTotalMatched;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public Date getMarketStartTime() {
        return marketStartTime;
    }

    public void setMarketStartTime(Date marketStartTime) {
        this.marketStartTime = marketStartTime;
    }

    public Double getTeam1BackPrice() {
        return team1BackPrice;
    }

    public void setTeam1BackPrice(Double team1BackPrice) {
        this.team1BackPrice = team1BackPrice;
    }

    public Double getTeam1BackVol() {
        return team1BackVol;
    }

    public void setTeam1BackVol(Double team1BackVol) {
        this.team1BackVol = team1BackVol;
    }

    public Double getTeam1LastPriceTraded() {
        return team1LastPriceTraded;
    }

    public void setTeam1LastPriceTraded(Double team1LastPriceTraded) {
        this.team1LastPriceTraded = team1LastPriceTraded;
    }

    public Double getTeam1LayPrice() {
        return team1LayPrice;
    }

    public void setTeam1LayPrice(Double team1LayPrice) {
        this.team1LayPrice = team1LayPrice;
    }

    public Double getTeam1LayVol() {
        return team1LayVol;
    }

    public void setTeam1LayVol(Double team1LayVol) {
        this.team1LayVol = team1LayVol;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public Long getTeam1SelectionId() {
        return team1SelectionId;
    }

    public void setTeam1SelectionId(Long team1SelectionId) {
        this.team1SelectionId = team1SelectionId;
    }

    public String getTeam1Status() {
        return team1Status;
    }

    public void setTeam1Status(String team1Status) {
        this.team1Status = team1Status;
    }

    public Double getTeam1TotalMatched() {
        return team1TotalMatched;
    }

    public void setTeam1TotalMatched(Double team1TotalMatched) {
        this.team1TotalMatched = team1TotalMatched;
    }

    public Double getTeam1TradedPrice() {
        return team1TradedPrice;
    }

    public void setTeam1TradedPrice(Double team1TradedPrice) {
        this.team1TradedPrice = team1TradedPrice;
    }

    public Double getTeam1TradedVol() {
        return team1TradedVol;
    }

    public void setTeam1TradedVol(Double team1TradedVol) {
        this.team1TradedVol = team1TradedVol;
    }

    public Double getTeam2BackPrice() {
        return team2BackPrice;
    }

    public void setTeam2BackPrice(Double team2BackPrice) {
        this.team2BackPrice = team2BackPrice;
    }

    public Double getTeam2BackVol() {
        return team2BackVol;
    }

    public void setTeam2BackVol(Double team2BackVol) {
        this.team2BackVol = team2BackVol;
    }

    public Double getTeam2LastPriceTraded() {
        return team2LastPriceTraded;
    }

    public void setTeam2LastPriceTraded(Double team2LastPriceTraded) {
        this.team2LastPriceTraded = team2LastPriceTraded;
    }

    public Double getTeam2LayPrice() {
        return team2LayPrice;
    }

    public void setTeam2LayPrice(Double team2LayPrice) {
        this.team2LayPrice = team2LayPrice;
    }

    public Double getTeam2LayVol() {
        return team2LayVol;
    }

    public void setTeam2LayVol(Double team2LayVol) {
        this.team2LayVol = team2LayVol;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public Long getTeam2SelectionId() {
        return team2SelectionId;
    }

    public void setTeam2SelectionId(Long team2SelectionId) {
        this.team2SelectionId = team2SelectionId;
    }

    public String getTeam2Status() {
        return team2Status;
    }

    public void setTeam2Status(String team2Status) {
        this.team2Status = team2Status;
    }

    public Double getTeam2TotalMatched() {
        return team2TotalMatched;
    }

    public void setTeam2TotalMatched(Double team2TotalMatched) {
        this.team2TotalMatched = team2TotalMatched;
    }

    public Double getTeam2TradedPrice() {
        return team2TradedPrice;
    }

    public void setTeam2TradedPrice(Double team2TradedPrice) {
        this.team2TradedPrice = team2TradedPrice;
    }

    public Double getTeam2TradedVol() {
        return team2TradedVol;
    }

    public void setTeam2TradedVol(Double team2TradedVol) {
        this.team2TradedVol = team2TradedVol;
    }

    public Double getTeamXBackPrice() {
        return teamXBackPrice;
    }

    public void setTeamXBackPrice(Double teamXBackPrice) {
        this.teamXBackPrice = teamXBackPrice;
    }

    public Double getTeamXBackVol() {
        return teamXBackVol;
    }

    public void setTeamXBackVol(Double teamXBackVol) {
        this.teamXBackVol = teamXBackVol;
    }

    public Double getTeamXLastPriceTraded() {
        return teamXLastPriceTraded;
    }

    public void setTeamXLastPriceTraded(Double teamXLastPriceTraded) {
        this.teamXLastPriceTraded = teamXLastPriceTraded;
    }

    public Double getTeamXLayPrice() {
        return teamXLayPrice;
    }

    public void setTeamXLayPrice(Double teamXLayPrice) {
        this.teamXLayPrice = teamXLayPrice;
    }

    public Double getTeamXLayVol() {
        return teamXLayVol;
    }

    public void setTeamXLayVol(Double teamXLayVol) {
        this.teamXLayVol = teamXLayVol;
    }

    public String getTeamXName() {
        return teamXName;
    }

    public void setTeamXName(String teamXName) {
        this.teamXName = teamXName;
    }

    public Long getTeamXSelectionId() {
        return teamXSelectionId;
    }

    public void setTeamXSelectionId(Long teamXSelectionId) {
        this.teamXSelectionId = teamXSelectionId;
    }

    public String getTeamXStatus() {
        return teamXStatus;
    }

    public void setTeamXStatus(String teamXStatus) {
        this.teamXStatus = teamXStatus;
    }

    public Double getTeamXTotalMatched() {
        return teamXTotalMatched;
    }

    public void setTeamXTotalMatched(Double teamXTotalMatched) {
        this.teamXTotalMatched = teamXTotalMatched;
    }

    public Double getTeamXTradedPrice() {
        return teamXTradedPrice;
    }

    public void setTeamXTradedPrice(Double teamXTradedPrice) {
        this.teamXTradedPrice = teamXTradedPrice;
    }

    public Double getTeamXTradedVol() {
        return teamXTradedVol;
    }

    public void setTeamXTradedVol(Double teamXTradedVol) {
        this.teamXTradedVol = teamXTradedVol;
    }

    public Double getTotalMatched() {
        return totalMatched;
    }

    public void setTotalMatched(Double totalMatched) {
        this.totalMatched = totalMatched;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.marketId);
        return hash;
    }

}
