package com.charlieparkerstraat.web;

import com.betfair.sports.api.MarketBook;
import com.betfair.sports.api.MarketCatalogue;
import com.betfair.sports.api.Runner;
import com.betfair.sports.api.RunnerCatalog;
import static com.charlieparkerstraat.ApplicationConstants.BETFAIR_JSON_DATE_FORMAT;
import static com.charlieparkerstraat.ApplicationConstants.MSG_BETFAIR_CLIENT_IS_NULL;
import static com.charlieparkerstraat.ApplicationConstants.MSG_BETFAIR_MESSAGE_TYPE_NOT_SUPPORTED;
import static com.charlieparkerstraat.ApplicationConstants.MSG_DISPATCHER_IS_NULL;
import static com.charlieparkerstraat.ApplicationConstants.MSG_USER_SESSION_IS_NULL;
import com.charlieparkerstraat.CacheManagerFactory;
import com.charlieparkerstraat.Dispatcher;
import com.charlieparkerstraat.ServerEndPointConfigurator;
import static com.charlieparkerstraat.ServerEndPointUtil.getAndRemoveHandshakeRequest;
import static com.charlieparkerstraat.ServerEndPointUtil.getBetfairConnection;
import static com.charlieparkerstraat.ServerEndPointUtil.getHttpSession;
import static com.charlieparkerstraat.ServerEndPointUtil.getServletContext;
import com.charlieparkerstraat.Subscriber;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpoint;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.charlieparkerstraat.betfair.client.BetfairClient;
import org.charlieparkerstraat.betfair.client.BetfairClientParameters;
import org.charlieparkerstraat.betfair.converters.JsonConverter;

@ServerEndpoint(value = "/markets", configurator = ServerEndPointConfigurator.class)
public class MarketsEndPoint implements Subscriber<MarketBook> {

    private static final AtomicInteger IDENTITY = new AtomicInteger();
    private static final java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(MarketsEndPoint.class.getName());
    private final JsonConverter JSON_CONVERTER = new JsonConverter(BETFAIR_JSON_DATE_FORMAT);
    private WeakReference<ServletContext> servletContextWeakReference;
    private final Set<UserSession> userSessions;

    /**
     * A new object is created for every client that opens a connection.
     */
    public MarketsEndPoint() {
        this.userSessions = new HashSet<>(0);
        LOG.log(Level.INFO, "object create with ID: {0}", IDENTITY.incrementAndGet());
    }

    /**
     * Dispatch is the method called by the dispatcher to send messages to the subscribers.
     *
     * @param <T>
     * @param message
     */
    @Override
    public <T> void dispatch(T message) {
        final BetfairClient<BetfairClientParameters> connection = getBetfairConnection(servletContextWeakReference.get());
        if (connection == null) {
            LOG.log(Level.WARNING, MSG_BETFAIR_CLIENT_IS_NULL);
            return;
        }
        if (!(message instanceof MarketBook)) {
            LOG.log(Level.WARNING, MSG_BETFAIR_MESSAGE_TYPE_NOT_SUPPORTED, new Object[]{message.getClass().getName()});
            return;
        }
        try {
            @SuppressWarnings("unchecked")
            final JsonMarket jsonMarket = load((MarketBook) message);
            LOG.log(Level.INFO, ToStringBuilder.reflectionToString(jsonMarket));
            final String json = JSON_CONVERTER.convertToJson(jsonMarket);
            LOG.log(Level.INFO, "to frontend: json: {0}", json);
            for (UserSession userSession : userSessions) {
                final Session session = userSession.getSession().get();
                if (session != null) {
                    session.getAsyncRemote().sendText(json);
                } else {
                    LOG.log(Level.WARNING, MSG_USER_SESSION_IS_NULL);
                }
            }
        } catch (Throwable ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    private JsonMarket load(final MarketBook book) {
        if (book == null) {
            LOG.log(Level.WARNING, "market book is null (not set)");
            return null;
        }
        final Cache catalogueCache = CacheManagerFactory.getCache(MarketCatalogue.class.getName());
        if (catalogueCache == null) {
            LOG.log(Level.WARNING, "market catalogue cache is null (not set)");
            return null;
        }
        final Element cataloguElement = catalogueCache.get(book.getMarketId());
        if (cataloguElement == null) {
            LOG.log(Level.WARNING, "market catalogue element is null (not set)");
            return null;
        }
        final MarketCatalogue catalogue = (MarketCatalogue) cataloguElement.getObjectValue();
        if (catalogue == null) {
            LOG.log(Level.WARNING, "market catalogue is null (not set)");
            return null;
        }
        if (catalogue.getEvent() == null) {
            LOG.log(Level.WARNING, "market catalogue event is null (not set)");
            return null;
        }
        final JsonMarket jsonMarket = new JsonMarket();
        jsonMarket.setEventId(catalogue.getEvent().getId());
        jsonMarket.setEventName(catalogue.getEvent().getName());
        jsonMarket.setEventOpenDate(catalogue.getEvent().getOpenDate());
        jsonMarket.setEventTimeZone(catalogue.getEvent().getTimezone());
        jsonMarket.setVenue(catalogue.getEvent().getVenue());
        jsonMarket.setCountryCode(catalogue.getEvent().getCountryCode());
        jsonMarket.setCompetitionId(catalogue.getCompetition().getId());
        jsonMarket.setCompetitionName(catalogue.getCompetition().getName());
        jsonMarket.setEventTypeId(catalogue.getEventType().getId());
        jsonMarket.setEventTypeName(catalogue.getEventType().getName());
        jsonMarket.setMarketId(catalogue.getMarketId());
        jsonMarket.setMarketName(catalogue.getMarketName());
        jsonMarket.setMarketStartTime(catalogue.getMarketStartTime());
        jsonMarket.setTotalMatched(catalogue.getTotalMatched());
        Long team1SelectionId = null;
        Long team2SelectionId = null;
        Long teamXSelectionId = null;
        for (RunnerCatalog runner : catalogue.getRunners()) {
            switch (runner.getSortPriority()) {
                case 1:
                    team1SelectionId = runner.getSelectionId();
                    jsonMarket.setTeam1SelectionId(runner.getSelectionId());
                    jsonMarket.setTeam1Name(runner.getRunnerName());
                    break;
                case 2:
                    team2SelectionId = runner.getSelectionId();
                    jsonMarket.setTeam2SelectionId(runner.getSelectionId());
                    jsonMarket.setTeam2Name(runner.getRunnerName());
                    break;
                case 3:
                    teamXSelectionId = runner.getSelectionId();
                    jsonMarket.setTeamXSelectionId(runner.getSelectionId());
                    jsonMarket.setTeamXName(runner.getRunnerName());
                    break;
            }
        }
        jsonMarket.setMarketBookBetDelay(book.getBetDelay());
        jsonMarket.setMarketBookComplete(book.getComplete());
        jsonMarket.setMarketBookCrossMatching(book.getCrossMatching());
        jsonMarket.setMarketBookInPlay(book.getInplay());
        jsonMarket.setMarketBookDelayedMarketData(book.getIsMarketDataDelayed());
        jsonMarket.setMarketBookStatus(book.getStatus());
        jsonMarket.setMarketBookTotalAvailable(book.getTotalAvailable());
        jsonMarket.setMarketBookTotalMatched(book.getTotalMatched());
        for (Runner runner : book.getRunners()) {
            if (team1SelectionId != null && team1SelectionId.equals(runner.getSelectionId())) {
                jsonMarket.setTeam1LastPriceTraded(runner.getLastPriceTraded());
                jsonMarket.setTeam1TotalMatched(runner.getTotalMatched());
                if (runner.getStatus() != null) {
                    jsonMarket.setTeam1Status(runner.getStatus().getDescription());
                }
                if (runner.getEx() != null && runner.getEx().getAvailableToBack() != null && !runner.getEx().getAvailableToBack().isEmpty()) {
                    jsonMarket.setTeam1BackPrice(runner.getEx().getAvailableToBack().get(0).getPrice());
                    jsonMarket.setTeam1BackVol(runner.getEx().getAvailableToBack().get(0).getSize());
                }
                if (runner.getEx() != null && runner.getEx().getAvailableToLay() != null && !runner.getEx().getAvailableToLay().isEmpty()) {
                    jsonMarket.setTeam1LayPrice(runner.getEx().getAvailableToLay().get(0).getPrice());
                    jsonMarket.setTeam1LayVol(runner.getEx().getAvailableToLay().get(0).getSize());
                }
                if (runner.getEx() != null && runner.getEx().getTradedVolume() != null && !runner.getEx().getTradedVolume().isEmpty()) {
                    jsonMarket.setTeam1TradedPrice(runner.getEx().getTradedVolume().get(0).getPrice());
                    jsonMarket.setTeam1TradedVol(runner.getEx().getTradedVolume().get(0).getSize());
                }
            } else if (team2SelectionId != null && team2SelectionId.equals(runner.getSelectionId())) {
                jsonMarket.setTeam2LastPriceTraded(runner.getLastPriceTraded());
                jsonMarket.setTeam2TotalMatched(runner.getTotalMatched());
                if (runner.getStatus() != null) {
                    jsonMarket.setTeam2Status(runner.getStatus().getDescription());
                }
                if (runner.getEx() != null && runner.getEx().getAvailableToBack() != null && !runner.getEx().getAvailableToBack().isEmpty()) {
                    jsonMarket.setTeam2BackPrice(runner.getEx().getAvailableToBack().get(0).getPrice());
                    jsonMarket.setTeam2BackVol(runner.getEx().getAvailableToBack().get(0).getSize());
                }
                if (runner.getEx() != null && runner.getEx().getAvailableToLay() != null && !runner.getEx().getAvailableToLay().isEmpty()) {
                    jsonMarket.setTeam2LayPrice(runner.getEx().getAvailableToLay().get(0).getPrice());
                    jsonMarket.setTeam2LayVol(runner.getEx().getAvailableToLay().get(0).getSize());
                }
                if (runner.getEx() != null && runner.getEx().getTradedVolume() != null && !runner.getEx().getTradedVolume().isEmpty()) {
                    jsonMarket.setTeam2TradedPrice(runner.getEx().getTradedVolume().get(0).getPrice());
                    jsonMarket.setTeam2TradedVol(runner.getEx().getTradedVolume().get(0).getSize());
                }
            } else if (teamXSelectionId != null && teamXSelectionId.equals(runner.getSelectionId())) {
                jsonMarket.setTeamXLastPriceTraded(runner.getLastPriceTraded());
                jsonMarket.setTeamXTotalMatched(runner.getTotalMatched());
                if (runner.getStatus() != null) {
                    jsonMarket.setTeamXStatus(runner.getStatus().getDescription());
                }
                if (runner.getEx() != null && runner.getEx().getAvailableToBack() != null && !runner.getEx().getAvailableToBack().isEmpty()) {
                    jsonMarket.setTeamXBackPrice(runner.getEx().getAvailableToBack().get(0).getPrice());
                    jsonMarket.setTeamXBackVol(runner.getEx().getAvailableToBack().get(0).getSize());
                }
                if (runner.getEx() != null && runner.getEx().getAvailableToLay() != null && !runner.getEx().getAvailableToLay().isEmpty()) {
                    jsonMarket.setTeamXLayPrice(runner.getEx().getAvailableToLay().get(0).getPrice());
                    jsonMarket.setTeamXLayVol(runner.getEx().getAvailableToLay().get(0).getSize());
                }
                if (runner.getEx() != null && runner.getEx().getTradedVolume() != null && !runner.getEx().getTradedVolume().isEmpty()) {
                    jsonMarket.setTeamXTradedPrice(runner.getEx().getTradedVolume().get(0).getPrice());
                    jsonMarket.setTeamXTradedVol(runner.getEx().getTradedVolume().get(0).getSize());
                }
            }
        }
        return jsonMarket;
    }

    /**
     * Remove any variables and references created to allow garbage collection.
     *
     * @param userSession
     */
    @OnClose
    public void onClose(Session userSession) {
        LOG.log(Level.INFO, "onClose - userSession.getId: {0}", this.getClass());
        final Dispatcher dispatcher = Dispatcher.getInstance();
        if (dispatcher != null) {
            dispatcher.unsubscribe(this, MarketBook.class);
        } else {
            LOG.log(Level.WARNING, MSG_DISPATCHER_IS_NULL);
        }
        for (Iterator<UserSession> iterator = userSessions.iterator(); iterator.hasNext();) {
            UserSession next = iterator.next();
            if (next.getSession().get() != null && next.getSession().get().equals(userSession)) {
                iterator.remove();
            }
        }
    }

    @OnMessage
    public void onMessage(String message, Session userSession) {
        LOG.log(Level.INFO, "onMessage - userSession.getId: {0}, message: {1}", new Object[]{this.getClass(), message});
        Map<Object, Element> bookElements = null;
        final Cache bookCache = CacheManagerFactory.getCache(MarketBook.class.getName());
        if (bookCache != null) {
            final List<?> keys = bookCache.getKeys();
            bookElements = bookCache.getAll(keys);
        }
        final List<JsonMarket> data = new ArrayList<>(0);
        if (bookElements != null && !bookElements.isEmpty()) {
            final int limit = 10;
            int count = 0;
            for (Object object : bookElements.values()) {
                if (count >= limit) {
                    break;
                }
                count++;
                final MarketBook book = (MarketBook) ((Element) object).getObjectValue();
                data.add(load(book));
            }
            final String json = JSON_CONVERTER.convertToJson(data);
            LOG.log(Level.INFO, "to frontend: json: {0}", json);
            userSession.getAsyncRemote().sendText(json);
        }
    }

    /**
     *
     * @param session
     * @param config
     */
    @OnOpen
    public void onOpen(final Session session, final EndpointConfig config) {
        LOG.log(Level.INFO, "onOpen - userSession.getId: {0}", this.getClass());
        final WeakReference<HandshakeRequest> threadLocalRequestWeakReference = new WeakReference<>(getAndRemoveHandshakeRequest());
        final HttpSession httpSession = getHttpSession(threadLocalRequestWeakReference.get());
        this.servletContextWeakReference = new WeakReference<>(getServletContext(httpSession));

        final UserSession userSession = new UserSession();
        userSession.setSession(new WeakReference<>(session));
        userSession.setHandshakeRequest(threadLocalRequestWeakReference);
        userSessions.add(userSession);

        final Dispatcher dispatcher = Dispatcher.getInstance();
        if (dispatcher != null) {
            dispatcher.subscribe(MarketBook.class, this);
        } else {
            LOG.log(Level.WARNING, MSG_DISPATCHER_IS_NULL);
        }
    }

    @Override
    public void subscribed() {
        LOG.log(Level.INFO, "subscribed");
    }

    @Override
    public void unsubscribed() {
        LOG.log(Level.INFO, "unsubscribed");
    }

    private static class UserSession {

        private WeakReference<HandshakeRequest> handshakeRequest;
        private WeakReference<Session> session;

        public WeakReference<HandshakeRequest> getHandshakeRequest() {
            return handshakeRequest;
        }

        public void setHandshakeRequest(WeakReference<HandshakeRequest> handshakeRequest) {
            this.handshakeRequest = handshakeRequest;
        }

        public WeakReference<Session> getSession() {
            return session;
        }

        public void setSession(WeakReference<Session> session) {
            this.session = session;
        }
    }
}
