/*
 * File: app/model/AccountStatementModel.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Dashboard.model.AccountStatementModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field',
        'Ext.ux.data.proxy.WebSocket'
    ],

    idProperty: 'refId',

    fields: [
        {
            name: 'refId'
        },
        {
            name: 'amount'
        },
        {
            name: 'balance'
        },
        {
            name: 'itemClass'
        },
        {
            name: 'itemDate'
        },
        {
            name: 'avgPrice'
        },
        {
            name: 'betCategoryType'
        },
        {
            name: 'betSize'
        },
        {
            name: 'betType'
        },
        {
            name: 'commissionRate'
        },
        {
            name: 'eventId'
        },
        {
            name: 'eventTypeId'
        },
        {
            name: 'fullMarketName'
        },
        {
            name: 'grossBetAmount'
        },
        {
            name: 'marketName'
        },
        {
            name: 'marketType'
        },
        {
            name: 'placedDate'
        },
        {
            name: 'selectionId'
        },
        {
            name: 'selectionName'
        },
        {
            name: 'startDate'
        },
        {
            name: 'transactionId'
        },
        {
            name: 'transactionType'
        },
        {
            name: 'winLose'
        }
    ],
    getDomainName: function () {
        console.log("AccountStatementModel#getDomainName");
        var matches = document.url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
        var domain = matches && matches[1];  // domain will be null if no match is found
        return domain;
    },
    proxy: {
        type: 'websocket',
        storeId: 'AccountStatementStore',
        url: 'ws://localhost:8084/webtrader/getaccountstatement',
//        url: 'ws://' + getDomainName() + ':8084/webtrader/getaccountstatement',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});